package com.chen.weikeup

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothSocket
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.*
import java.io.IOException
import java.io.PrintStream
import java.util.*

class MainActivity : AppCompatActivity() {

    private val btnRefreshBTList: Button by lazy { findViewById<Button>(R.id.refresh_bt_list) }
    private val btDeviceListView: ListView by lazy { findViewById<ListView>(R.id.bt_list) }
    private val btnSendOne: Button by lazy { findViewById<Button>(R.id.send_one) }
    private val textDataView: TextView by lazy { findViewById<TextView>(R.id.text_data) }
    private val btDataListView: ListView by lazy { findViewById<ListView>(R.id.bt_data_list) }
    private val btnSendData: Button by lazy { findViewById<Button>(R.id.send_data) }
    private val btnBtDisconnect: Button by lazy { findViewById<Button>(R.id.bt_disconnect) }

    private val btStateChangedHandle = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.d(TAG, "Bluetooth State: ${BluetoothAdapter.getDefaultAdapter().state}")
        }
    }

    private val btAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
    private var btSocket: BluetoothSocket? = null

    private val TAG = "MainActivity"
    private val REQUEST_BT_ENABLE = 1
    private val MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRefreshBTList.setOnClickListener {

            if (btAdapter?.isEnabled == false) {
                Log.d(TAG, "Start Request Bluetooth Enable Activity")
                startActivityForResult(
                    Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                    REQUEST_BT_ENABLE
                )
            } else {
                val btDevices = btAdapter?.bondedDevices?.toList()
                btAdapter?.cancelDiscovery()
                val list = ArrayList<Map<String, String>>()

                btDevices?.forEach {
                    list.add(mapOf("name" to it.name, "address" to it.address))
                }

                val listAdapter = SimpleAdapter(
                    this,
                    list,
                    android.R.layout.simple_list_item_2,
                    arrayOf("name", "address"),
                    intArrayOf(android.R.id.text1, android.R.id.text2)
                )

                btDeviceListView.adapter = listAdapter
                btDeviceListView.setOnItemClickListener { parent, view, position, id ->
                    val selectedItemStr = listAdapter.getItem(position)
                    Log.d(TAG, "btDeviceListView.ItemClick-> pos: $position -> $selectedItemStr")

                    val selectedItem = btDevices?.get(position)
                    AlertDialog.Builder(this).setTitle("連線？")
                        .setMessage("要連線到 ${selectedItem?.name}？")
                        .setPositiveButton("連線") { dialog, which ->
                            Log.d(TAG, "Connect to $selectedItemStr")

                            btAdapter?.cancelDiscovery()

                            if (btSocket?.isConnected == true) btSocket?.close()
                            btSocket = selectedItem?.createRfcommSocketToServiceRecord(MY_UUID)

                            Toast.makeText(this, "連線中... ${selectedItem?.name}", Toast.LENGTH_SHORT)
                                .show()

                            AsyncTask.execute {
                                try {
                                    btSocket?.connect()
                                    runOnUiThread {
                                        Toast.makeText(
                                            this,
                                            "連線成功 ${selectedItem?.name}",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        AsyncTask.execute {
                                            val listAdapter =
                                                ArrayAdapter<String>(
                                                    this,
                                                    android.R.layout.simple_list_item_1
                                                )
                                            runOnUiThread {
                                                btDataListView.adapter = listAdapter
                                            }
                                            val sca = Scanner(btSocket?.inputStream)
                                            while (btSocket?.isConnected == true) {
                                                Log.d(TAG, "Wait BT Data")
                                                try {
                                                    val inStr =
                                                        if (sca.hasNextLine()) sca.nextLine() else null
                                                    inStr?.let {
                                                        Log.d(TAG, "Get BT Data: $inStr")
                                                        runOnUiThread {
                                                            listAdapter.insert(inStr, 0)
                                                        }
                                                    }
                                                } catch (ex: Exception) {
                                                    Log.e(TAG, "BT Read Error: ", ex)
                                                }
                                            }
                                        }
                                    }

                                    Log.d(TAG, "btSocket.isConnected: ${btSocket?.isConnected}")

                                } catch (ex: IOException) {
                                    runOnUiThread {
                                        Toast.makeText(this, "連線失敗", Toast.LENGTH_SHORT).show()
                                    }
                                    Log.e(TAG, "Bluetooth Connect Fail: ", ex)
                                }
                            }
                        }
                        .setNegativeButton("取消") { dialog, which -> dialog.cancel() }
                        .show()
                }
            }
        }

        btnSendOne.setOnClickListener {
            if (btSocket?.isConnected == true) {
                val ps = PrintStream(btSocket?.outputStream)
                ps.print(1)
                ps.flush()
            }
        }

        btnSendData.setOnClickListener {
            if (btSocket?.isConnected == true) {
                val ps = PrintStream(btSocket?.outputStream)
                ps.println(textDataView.text)
                ps.flush()
            }
        }

        btnBtDisconnect.setOnClickListener {
            if (btSocket?.isConnected == true) btSocket?.close()
        }

    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume()")
        registerReceiver(btStateChangedHandle, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "onActivityResult()-> rqc: $requestCode, rc: $resultCode, data: $data")

        when (requestCode) {
            REQUEST_BT_ENABLE -> {
                if (resultCode == Activity.RESULT_OK) {
                    btnRefreshBTList.callOnClick()
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause()")
        unregisterReceiver(btStateChangedHandle)
    }

    override fun onStop() {
        super.onStop()
        btSocket?.close()
    }
}
